#################################################################

############ A4D - Single-script version ######################

#################################################################

# This script is a recopilatory of the functions needed to execute
# A4D-fMRI (Constantini et al, 2022) in the cluster using a list of
# subjects. 
# It includes the corrections to delete the variables of denoise
# after they are no longer required to save memory

#################################################################

#################################################################


from itertools import product
import numpy as np
import tensorflow as tf
from scipy.ndimage.filters import gaussian_filter
import argparse 
parser = argparse.ArgumentParser()
parser.add_argument('id') #subject id number
args = parser.parse_args()
id = args.id

 
def compute_gradients(image):
    return np.array(
        [np.diff(image, append=0, axis=i) for i in range(image.ndim)], dtype=np.float32)

 
def gaussian(x):
    return np.exp(-x ** 2 / (2 * 0.1 ** 2))

 
def denoise(image):

    print("Computing image gradients")
    gradients = compute_gradients(image)
    #print(gradients.dtype)
    print("Computed gradients//finished")

    print("Computing structure tensor for each voxel")
    ndim = image.ndim
    structure_tensor = np.zeros(image.shape + (ndim, ndim), dtype=np.float32)
    #print(structure_tensor.dtype)
    for i, j in product(range(ndim), repeat=2):
        if j > i:
            continue
        structure_tensor[..., i, j] = gaussian_filter(
            gradients[i] * gradients[j], 1)
    print("Computed structure tensor//finished")
    
    print("Decomposition of the structure tensor")
    eigenvalues = np.empty(image.shape + (image.ndim,), dtype=np.float32)
    #print(eigenvalues.dtype)
    eigenvectors = np.empty(image.shape + (image.ndim, image.ndim), dtype=np.float32)
    #print(eigenvectors.dtype)
    for sample in range(image.shape[3]):
        eigenvalues[:, :, :, sample, :], eigenvectors[:, :, :, sample, :, :] =\
            tf.linalg.eigh(structure_tensor[:, :, :, sample, :, :])
    del structure_tensor #releasing memory
    print("Decomposed structure tensor//finished")

    # Set the maximum eigenvalue of each voxel using a Gaussian and all others
    # to 1. Because we use eigh instead of eig, they eigenvalues are in
    # ascending order.
    print("Rebuilding structure tensor")
    new_eigenvalues = np.ones_like(eigenvalues)
    #print(new_eigenvalues.dtype)
    new_eigenvalues[..., -1] = gaussian(
        eigenvalues[..., -1] / np.max(eigenvalues))
    del eigenvalues
    weighted_eigenvectors = eigenvectors * new_eigenvalues[..., None, :]
    del new_eigenvalues
    # keep doing, do for whole file
    #array.nbytes
    print(weighted_eigenvectors.dtype)
    new_structure_tensor = np.matmul(
        weighted_eigenvectors,
        np.transpose(eigenvectors, (0, 1, 2, 3, 5, 4)))
    del eigenvectors
    del weighted_eigenvectors
    #print(new_structure_tensor.dtype)
    print("Rebuilt structure tensor//finished")

    print("Computing the oriented gradients")
    oriented_gradient = np.matmul(
        new_structure_tensor,
        gradients.transpose((1, 2, 3, 4, 0))[..., None])
    oriented_gradient = np.squeeze(oriented_gradient, axis=-1)
    #print(oriented_gradient.dtype)
    print("Computing the oriented gradients//finished")


    print("Computing divergence of oriented gradients.")
    divergence = np.sum(
        [np.diff(oriented_gradient[..., i], prepend=0, axis=i)
         for i in range(ndim)],
        axis=0, dtype = np.float32)
    print("Computed divergence of oriented gradients//finished")
    #print(divergence.dtype)
    del oriented_gradient
    return divergence


#################################################################################################


##################################################################################################


## CODE FOR THE HEMODYNAMIC RESPONSE FUNCTION FILTER --> hrf_operators.py


import numpy as np

# Definition of constants from Khalidov et al. 2011
# Activelets: Wavelets for sparse representation of hemodynamic responses

epsilon = 0.54
tau_s = 1.54
tau_f = 2.46
tau0 = 0.98
alpha = 0.33
e0 = 0.34
v0 = 1
k1 = 7 * e0
k2 = 2
k3 = 2 * e0 - 0.2
c = (1 + ((1 - e0) * np.log(1 - e0)) / e0) / tau0

mult_coeff = 1  # multiplicative coefficient

S = v0 * epsilon / tau0 * (
            -(k1 + k2) * c * tau0 - k3 + k2)  # scaling factor has to be applied to the signal and not to the roots
gamma1 = ((k1 + k2) * ((1 - alpha) / (alpha * tau0) - c / alpha) - (k3 - k2) / tau0) / \
         (- (k1 + k2) * c * tau0 - k3 + k2)

alpha1 = mult_coeff / tau0
alpha2 = mult_coeff / (alpha * tau0)
alpha3 = mult_coeff / (2 * tau_s) * (1 + 1j * np.sqrt(4 * (tau_s ** 2) / tau_f - 1))
alpha4 = mult_coeff / (2 * tau_s) * (1 - 1j * np.sqrt(4 * (tau_s ** 2) / tau_f - 1))


def hrf(u):
    """Inputs:
    u = input signal (piece-wise constant)

    Outputs:
    This function, appplied to a Dirac impulse gives as output the BOLD response
    (HRF) following the papaer of Khalidov et al., 2011.
    """

    x1, x2, x3, x4 = 0, 0, 0, 0
    bold_linear_list = []

    for i in range(0, len(u)):
        delta_x1 = epsilon * u[i] - x1 / tau_s + x2 / tau_f
        delta_x2 = - x1
        delta_x3 = (x2 - (x3 / alpha)) / tau0
        delta_x4 = c * x2 - ((1 - alpha) / (alpha * tau0)) * x3 - x4 / tau0

        x1 = x1 + delta_x1 * mult_coeff
        x2 = x2 + delta_x2 * mult_coeff
        x3 = x3 + delta_x3 * mult_coeff
        x4 = x4 + delta_x4 * mult_coeff

        bold_linear = v0 * ((k1 + k2) * x4 + (k3 - k2) * x3)
        bold_linear_list.append(bold_linear)
    return bold_linear_list


def h_operator(x):
    """Implementation of H operator as in VDV paper
    x = input signal
    gamma1 = zero of the transfer function L
    alpha1-4 = poles of the transfer function H
    S = scaling factor"""
# Changed: from complex64 to float32
    y = np.zeros_like(x, dtype=np.complex64)

    for n in range(1, len(x)):
        y[n] = x[n] - np.exp(-gamma1) * x[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha1) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha2) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha3) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha4) * y[n - 1]

    y = np.real(y * S)
    return y

# ATTENTION: the following function used to be called hrf_operator_TR_adjusted
# it is now renamed for simplicity
 
def hrf_op(x, TR):
    """Implementation of H operator as in VDV paper
    x = input signal
    gamma1 = zero of the transfer function L
    alpha1-4 = poles of the transfer function H
    S = scaling factor"""
    y = np.zeros_like(x, dtype=np.complex64)

    for n in range(1, len(x)):
        y[n] = x[n] - np.exp(-gamma1 * TR) * x[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha1 * TR) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha2 * TR) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha3 * TR) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha4 * TR) * y[n - 1]

    y = np.real(y * S)
    return y


##########################################################################################################################


##########################################################################################################################

from scipy.ndimage.filters import convolve1d


## Here we will write a4dfmri.denoise, renamed as:
 
def a4dfmri_denoise(
        image: np.ndarray,
        hemodynamic_response=None,
        n_iterations: int = 1,
        regularization_weight: float = 0.5,
        step_size: float = 0.01) -> np.ndarray:

    # If the hemodynamic response is not provided, use the identity.
    if hemodynamic_response is None:
        hemodynamic_response = np.ones((1,))

    # Denoise the image iteratively using the available implementation.
    denoised_image = image.copy()
    for i in range(n_iterations):
        regularization = denoise(denoised_image)
        data_fit = convolve1d(
            (image - convolve1d(denoised_image, hemodynamic_response, axis=-1)),
            hemodynamic_response[::-1], axis=-1)
 
        change = regularization_weight * regularization + \
                 (1.0 - regularization_weight) * data_fit
        denoised_image += step_size * change

    return denoised_image

##########################################################################################################################


##########################################################################################################################

## Main function: aggregating all the previous code to generate a processed image

import nibabel as nib

 
def main_function():
    
    nii = nib.load(f'path/to/input/data/with/id/number')  
    image = np.array(nii.get_fdata(), dtype=np.float32)

    
    repetition_time=nii.header.get_zooms()[-1]

    
    nb_iterations = 100


    # Compute the HRF operator
    t_size = np.size(image, 3)
    dirac = np.zeros((t_size,), )
    dirac[t_size // 2 + 1] = 1
    hrf = hrf_op(dirac, repetition_time)


    denoised_image = a4dfmri_denoise(image, hrf, n_iterations=nb_iterations)
    
    nib.save(nib.Nifti1Image(denoised_image, nii.affine), f'/path/to/output/data/with/id/number')


        


main_function()
        
