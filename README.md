# A4D Report Resources

This project contains the single-script used execute the data processing of A4D-fMRI (Constantini et al, 2022) in the cluster, a series of Notebooks that generate windows with sliders to visualice the effect of multiple iterations on each of the variables in the a4dfmri_denoise function, and a compressed set of anonymized and processed data to execute them.